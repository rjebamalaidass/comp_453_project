from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from flask_login import current_user
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField, IntegerField, DateField, SelectField, HiddenField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError,Regexp
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from flaskDemo import db
from flaskDemo.models import User
from wtforms.fields.html5 import DateField


class ViewCartForm(FlaskForm):
    address1 = StringField('Address 1',  validators=[DataRequired(), Length(min=2, max=100)])
    address2 = StringField('Address 2',  validators=[Length(min=2, max=100)])
    city = StringField('City',  validators=[DataRequired(), Length(min=2, max=50)])
    state = StringField('State',  validators=[DataRequired(), Length(min=2, max=20)])
    zip = StringField('Zip',  validators=[DataRequired(), Length(min=2, max=20)])
    orderid = HiddenField("orderid")
    submit = SubmitField("Place Order")

class RegistrationForm(FlaskForm):
    username = StringField('Username',
                           validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[DataRequired(), EqualTo('password')])
    type = SelectField(u'Type of User', choices=[('C', 'Customer'), ('E', 'Employee')])
    address1 = StringField('Address 1',  validators=[DataRequired(), Length(min=2, max=100)])
    address2 = StringField('Address 2',  validators=[DataRequired(), Length(min=2, max=100)])
    city = StringField('City',  validators=[DataRequired(), Length(min=2, max=50)])
    state = StringField('State',  validators=[DataRequired(), Length(min=2, max=20)])
    zip = StringField('Zip',  validators=[DataRequired(), Length(min=2, max=20)])
    submit = SubmitField('Sign Up')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('That username is taken. Please choose a different one.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('That email is taken. Please choose a different one.')



class LoginForm(FlaskForm):
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')


class PostForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    content = TextAreaField('Content', validators=[DataRequired()])
    submit = SubmitField('Post')

    
class DeptUpdateForm(FlaskForm):

#    dnumber=IntegerField('Department Number', validators=[DataRequired()])
    dnumber = HiddenField("")

    dname=StringField('Department Name:', validators=[DataRequired(),Length(max=15)])
#  Commented out using a text field, validated with a Regexp.  That also works, but a hassle to enter ssn.
#    
#    mgr_start=DateField("Manager's Start Date", format='%Y-%m-%d')
    mgr_start = DateField("Manager's start date:", format='%Y-%m-%d')  # This is using the html5 date picker (imported)
    submit = SubmitField('Update this department')



