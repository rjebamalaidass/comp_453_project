from datetime import datetime
from flaskDemo import db, login_manager
from flask_login import UserMixin
from functools import partial
from sqlalchemy import orm

db.Model.metadata.reflect(db.engine)

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
   __table_args__ = {'extend_existing': True}
   id = db.Column(db.Integer, primary_key=True)
   username = db.Column(db.String(20), unique=True, nullable=False)
   email = db.Column(db.String(40), unique=True, nullable=False)
   password = db.Column(db.String(25), nullable=False)
   type = db.Column(db.String(10), nullable=True)
   is_active = db.Column(db.String(1), nullable=False,default='y')
    

   def __repr__(self):
    return f"User('{self.username}', '{self.email}', '{self.image_file}')"




class Address(db.Model):
   __table__ = db.Model.metadata.tables['address']


class Order(db.Model):
   __table__ = db.Model.metadata.tables['order1']
        

class OrderDetail(db.Model):
    __table__ = db.Model.metadata.tables['order_detail']

class PartnerOrder(db.Model):
    __table__ = db.Model.metadata.tables['partner_order']

class Payment(db.Model):
    __table__ = db.Model.metadata.tables['payment']

class Product(db.Model):
    __table__ = db.Model.metadata.tables['product']
    #productid = db.Column(db.Integer, primary_key=True)
    #userid = db.Column(db.Integer)
    #productname = db.Column(db.String(50), nullable=False)
    #attributename = db.Column(db.String(50))
    #description = db.Column(db.String(250))
    #category = db.Column(db.String(25))
    #unitprice = db.Column(db.String(7))
    #availablequantity = db.Column(db.Integer)

    

    
class Shipping(db.Model):
    __table__ = db.Model.metadata.tables['shipping']



    

  
